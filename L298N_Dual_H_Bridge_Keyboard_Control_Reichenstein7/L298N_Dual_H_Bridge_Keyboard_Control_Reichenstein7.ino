//Code by Reichenstein7 (thejamerson.com)

//Keyboard Controls:
//
// 1 -Motor 1 Left
// 2 -Motor 1 Stop
// 3 -Motor 1 Right
//
// 4 -Motor 2 Left
// 5 -Motor 2 Stop
// 6 -Motor 2 Right

// Declare L298N Dual H-Bridge Motor Controller directly since there is not a library to load.

// Motor 1
int dir1PinA = 2;
int dir2PinA = 3;
int speedPinA = 9; // Needs to be a PWM pin to be able to control motor speed

// Motor 2
int dir1PinB = 4;
int dir2PinB = 5;
int speedPinB = 10; // Needs to be a PWM pin to be able to control motor speed

int maxSpeed = 110;

void setup() {  
  // Setup runs once per reset
  // initialize serial communication @ 9600 baud:
  Serial.begin(9600);
  
  //Define L298N Dual H-Bridge Motor Controller Pins
  
  pinMode(dir2PinA,OUTPUT);
  pinMode(dir1PinA,OUTPUT);
  pinMode(speedPinA,OUTPUT);
  pinMode(dir2PinB,OUTPUT);
  pinMode(dir1PinB,OUTPUT);
  pinMode(speedPinB,OUTPUT);

  Serial.println("started"); 
}

void loop() {

  // Initialize the Serial interface:
  
  if (Serial.available() > 0) {
    int inByte = Serial.read();
    int speed; // Local variable

    switch (inByte) {
    
    //______________Forward______________
    case 'f': 
    case 'F': 
      // All motors Forward
      analogWrite(speedPinA, maxSpeed);//Sets speed variable via PWM 
      digitalWrite(dir1PinA, LOW);
      digitalWrite(dir2PinA, HIGH);
      
      analogWrite(speedPinB, maxSpeed);
      digitalWrite(dir1PinB, LOW);
      digitalWrite(dir2PinB, HIGH);
      Serial.println("Motor 1 & 2 Forward"); 
      break;

    //______________Stop______________  
    case 's': 
    case 'S': 
      // Motor 1 Stop (Freespin)
      analogWrite(speedPinA, 0);
      digitalWrite(dir1PinA, LOW);
      digitalWrite(dir2PinA, HIGH);
      
      analogWrite(speedPinB, 0);
      digitalWrite(dir1PinB, LOW);
      digitalWrite(dir2PinB, HIGH);
      Serial.println("Motor 1 & 2 Stop");
      break;
    
    //______________Back______________
    case 'b': 
    case 'B': 
      // Motor 1 Back
      analogWrite(speedPinA, maxSpeed);
      digitalWrite(dir1PinA, HIGH);
      digitalWrite(dir2PinA, LOW);
      
      analogWrite(speedPinB, maxSpeed);
      digitalWrite(dir1PinB, HIGH);
      digitalWrite(dir2PinB, LOW);
      
      Serial.println("Motor 1 & 2 Reverse");
      break;
      
    //______________Left______________
    case 'l': 
    case 'L': 
      // Left motor half speed, right motor full speed
      analogWrite(speedPinA, maxSpeed / 2);//Sets speed variable via PWM 
      digitalWrite(dir1PinA, LOW);
      digitalWrite(dir2PinA, HIGH);
      
      analogWrite(speedPinB, maxSpeed);
      digitalWrite(dir1PinB, LOW);
      digitalWrite(dir2PinB, HIGH);
      
      Serial.println("Motor 1 & 2 Forward"); 
      break;
      
    //______________Right______________
    case 'r': 
    case 'R': 
      // Right motor half speed, left motor full speed
      analogWrite(speedPinA, maxSpeed);//Sets speed variable via PWM 
      digitalWrite(dir1PinA, LOW);
      digitalWrite(dir2PinA, HIGH);
      
      analogWrite(speedPinB, maxSpeed / 2);
      digitalWrite(dir1PinB, LOW);
      digitalWrite(dir2PinB, HIGH);
      
      Serial.println("Motor 1 & 2 Forward"); 
      break;

    default:
      // turn all the connections off
      for (int thisPin = 2; thisPin < 11; thisPin++) {
        digitalWrite(thisPin, LOW);
      }
    }    
  }
}

