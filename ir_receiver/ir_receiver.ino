#include <IRremote.h>
#include <IRremoteInt.h>

//#include <IRremote.h>
//#include <IRremoteInt.h>
//#include <IRremoteTools.h>

// Define a few commands from your remote control
#define IR_CODE_FORWARD 284154405
#define IR_CODE_BACKWARDS 284113605
#define IR_CODE_TURN_LEFT 284129925
#define IR_CODE_TURN_RIGHT 284127885
#define IR_CODE_CONTINUE -1

int TKD2 = 11;

void setup() {
  Serial.begin(9600);

  beginIRremote(); // Start the receiver

}

void loop() {
  // if there is an IR command, process it
  if (IRrecived()) {
    processResult();
    resumeIRremote(); // resume receiver
  }

}

void processResult() {
  unsigned long res = getIRresult();
  switch (res) {
    case IR_CODE_FORWARD:
      //changeAction(1, 1); //Move the robot forward
      Serial.println("IR_CODE_FORWARD");
      break;
    case IR_CODE_BACKWARDS:
      //changeAction(-1, -1); //Move the robot backwards
      Serial.println("IR_CODE_BACKWARDS");
      break;
    case IR_CODE_TURN_LEFT:
      //changeAction(-0.5, 0.5); //Turn the robot left
      Serial.println("IR_CODE_TURN_LEFT");
      break;
    case IR_CODE_TURN_RIGHT:
      //changeAction(0.5, -0.5); //Turn the robot Right
      Serial.println("IR_CODE_TURN_RIGHT");
      break;
    case IR_CODE_CONTINUE:
      //timer = millis(); //Continue the last action, reset timer
      Serial.println("IR_CODE_CONTINUE");
      break;
  }
}
