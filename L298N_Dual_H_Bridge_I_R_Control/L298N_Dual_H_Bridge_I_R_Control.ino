

#include <IRremote.h>
#include <Servo.h>

/**
 HC-SR04 Ping distance sensor]
 VCC to arduino 5v GND to arduino GND
 Echo to Arduino pin 13 Trig to Arduino pin 12
 */

#define trigPin 13
#define echoPin 12

#define IR_RECV_PIN 11

#define SERVO_PIN 9

#define SERVO_POS_AHEAD 105

IRrecv irrecv(IR_RECV_PIN);
decode_results results;

Servo myservo;  // create servo object to control a servo

//Keyboard Controls:
//
// 1 -Motor 1 Left
// 2 -Motor 1 Stop
// 3 -Motor 1 Right
//
// 4 -Motor 2 Left
// 5 -Motor 2 Stop
// 6 -Motor 2 Right

// Declare L298N Dual H-Bridge Motor Controller directly since there is not a library to load.

// PWM PINS ARE 3, 5, 6, 9, 10, and 11

// Motor 1
int dir1PinA = 2;
int dir2PinA = 3;
int speedPinA = 5; // Needs to be a PWM pin to be able to control motor speed

// Motor 2
int dir1PinB = 4;
int dir2PinB = 7;
int speedPinB = 6; // Needs to be a PWM pin to be able to control motor speed

int maxSpeed = 200;

// counter for distance sensor
int dist_sensor_counter = 0;

// distance to obstruction
long distance = 0;

// store the servo position
int servo_pos = SERVO_POS_AHEAD;    

// current direction of travel, starts at stopped position
char current_action = 's';


void setup() {  
  // Setup runs once per reset
  // initialize serial communication @ 9600 baud:
  Serial.begin(9600);

  //distance sensor setup
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  // attaches the servo on pin 9 to the servo object
  myservo.attach(SERVO_PIN);  

  // Start the IR receiver
  irrecv.enableIRIn(); 
  
  //Define L298N Dual H-Bridge Motor Controller Pins
  
  pinMode(dir2PinA,OUTPUT);
  pinMode(dir1PinA,OUTPUT);
  pinMode(speedPinA,OUTPUT);
  pinMode(dir2PinB,OUTPUT);
  pinMode(dir1PinB,OUTPUT);
  pinMode(speedPinB,OUTPUT);

  Serial.println("started"); 
}

void loop() {

  if (dist_sensor_counter < 5) {
    dist_sensor_counter += 1;
  } else {
    Serial.println(char(current_action));
    dist_sensor_counter = 0;

    if (myservo.read() != servo_pos) {
      // tell servo to go to position in variable 'pos'
      myservo.write(servo_pos);              
      // waits for the servo to reach the position
      delay(50);                       
    }

    distance = distance_to_obstruction();
    Serial.println(distance);
  }

  char next_action = ir_code_to_action();

  // If there is no action from remote, continue doing current
  if (next_action == 'u') {
    next_action = current_action;
  }

  // If we are too close to something, stop unless we are backing off
  if (distance < 15 && (next_action != 'b' || current_action != 'b')){
    next_action = 's'; 
  }
  
  if (current_action != next_action) {
    control_motors(next_action);
  }
   
  delay(100);
}

char ir_code_to_action() {
  char next_action = 'u';
  while (irrecv.decode(&results) && next_action == 'u') {
    Serial.println(results.value, HEX);

    switch (results.value) {
      case 0xFF18E7://forward =2
      case 0x3D9AE3F7:
        next_action = 'f';
        break;

      case 0xFF4AB5://8 = back
      case 0x1BC0157B:
        next_action = 'b';
        break;
     
      case 0xFF10EF://left =4
      case 0x8C22657B:
      case 0xF825842D:
        next_action = 'l';
        break;
      
      case 0xFF5AA5://right =6
      case 0x449E79F:
      case 0xA6BBDDB1:
         next_action = 'r';
        break;
        
      case 0xFFFFFFFF:
        next_action = current_action;
        break;

      default:
        next_action = 's';
        break;
    }

    irrecv.resume(); // Receive the next value
  }

  return next_action;
}

long distance_to_obstruction() {
  long duration, distance;
  digitalWrite(trigPin, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigPin, HIGH);

  delayMicroseconds(10); // Added this line
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2) / 29.1;

  return distance;
}

void control_motors(char action) {

    int speed; // Local variable

    current_action = action;

    switch (action) {
    
    //______________Forward______________
    case 'f': 
      // All motors Forward
      analogWrite(speedPinA, maxSpeed);//Sets speed variable via PWM 
      digitalWrite(dir1PinA, LOW);
      digitalWrite(dir2PinA, HIGH);
      
      analogWrite(speedPinB, maxSpeed);
      digitalWrite(dir1PinB, LOW);
      digitalWrite(dir2PinB, HIGH);
      Serial.println("Motor 1 & 2 Forward"); 
      break;

    //______________Stop______________  
    case 's': 
      // Motor 1 Stop (Freespin)
      analogWrite(speedPinA, 0);
      digitalWrite(dir1PinA, LOW);
      digitalWrite(dir2PinA, HIGH);
      
      analogWrite(speedPinB, 0);
      digitalWrite(dir1PinB, LOW);
      digitalWrite(dir2PinB, HIGH);
      Serial.println("Motor 1 & 2 Stop");
      break;
    
    //______________Back______________
    case 'b': 
      // Motor 1 Back
      analogWrite(speedPinA, maxSpeed);
      digitalWrite(dir1PinA, HIGH);
      digitalWrite(dir2PinA, LOW);
      
      analogWrite(speedPinB, maxSpeed);
      digitalWrite(dir1PinB, HIGH);
      digitalWrite(dir2PinB, LOW);
      
      Serial.println("Motor 1 & 2 Reverse");
      break;
      
    //______________Left______________
    case 'l': 
      // Left motor half speed, right motor full speed
      analogWrite(speedPinA, maxSpeed / 3);//Sets speed variable via PWM 
      digitalWrite(dir1PinA, LOW);
      digitalWrite(dir2PinA, HIGH);
      
      analogWrite(speedPinB, maxSpeed);
      digitalWrite(dir1PinB, LOW);
      digitalWrite(dir2PinB, HIGH);
      
      Serial.println("Motor 1 & 2 Left"); 
      break;
      
    //______________Right______________
    case 'r': 
      // Right motor half speed, left motor full speed
      analogWrite(speedPinA, maxSpeed);//Sets speed variable via PWM 
      digitalWrite(dir1PinA, LOW);
      digitalWrite(dir2PinA, HIGH);
      
      analogWrite(speedPinB, maxSpeed / 3);
      digitalWrite(dir1PinB, LOW);
      digitalWrite(dir2PinB, HIGH);
      
      Serial.println("Motor 1 & 2 Right"); 
      break;

    case 'u':
      // don't do anything
      break;

    default:
      // turn all the connections off
      for (int thisPin = 2; thisPin < 11; thisPin++) {
        digitalWrite(thisPin, LOW);
      }
    }    

}

