# README #

Arduino sketches and other info for running the DimeArduino [Ultimate Car Kit](https://www.dimearduino.com.au/collections/top-selling/products/super-deluxe-arduino-car-kit)
![product-image-116511059_1024x1024.jpg](https://bitbucket.org/repo/X4E6eL/images/2619494457-product-image-116511059_1024x1024.jpg)

A more sophisticated one on [Amazon](https://www.amazon.com/Elegoo-Four-wheel-Ultrasonic-Intelligent-Educational/dp/B01DPH0SWY)



### Information ###

#### Videos

* https://www.youtube.com/watch?v=Lf7JJAAZxEU
* https://www.youtube.com/watch?v=jQpwQMGu7Yk

#### L298N Dual H-Bridge Motor Controller

* http://www.instructables.com/id/Arduino-Modules-L298N-Dual-H-Bridge-Motor-Controll/step2/Wiring-to-an-Arduino/
* http://www.canakit.com/Media/Manuals/UK1122.pdf


#### IR Receiver

* The library we used
** https://github.com/z3t0/Arduino-IRremote
* https://arduino-info.wikispaces.com/IR-RemoteControl
* https://learn.adafruit.com/using-an-infrared-library/hardware-needed

#### IR Tracking Sensor

* https://www.sunfounder.com/learn/Sensor-Kit-v2-0-for-Arduino/lesson-30-ir-tracking-sensor-sensor-kit-v2-0-for-arduino.html
* https://www.sunfounder.com/wiki/index.php?title=IR_Tracking_Sensor_Module

#### Sensor Shield V4

* https://arduino-info.wikispaces.com/SensorShield

#### Ultrasound Sensor

* http://www.instructables.com/id/Simple-Arduino-and-HC-SR04-Example/?ALLSTEPS

### Other additions

#### OLED 128x64 Screen

* [Description for the one from eBay](https://bitbucket.org/francois_fernando/arduino-sketch/wiki/OLED%20Display%20from%20eBay)
* https://learn.adafruit.com/monochrome-oled-breakouts/wiring-1-dot-3-128x64
* [Google arduino lcd library oled](https://www.google.com.au/search?q=arduino+lcd+library+oled&oq=arduino+lcd+library+oled)
* [Arduino OLED Display Library - Oscar Liang](https://oscarliang.com/arduino-oled-display-library/)
* [Universal Library to drive OLED and LCD](https://hallard.me/driving-oled-lcd/)



#### Filtering sensor data

Distance sensor seems to give spurious readings some times. It might be possible use a Kalman Filter on distance sensor data to weed out such values (Google Arduino Kalman Filter). 


* http://interactive-matter.eu/blog/2009/12/18/filtering-sensor-data-with-a-kalman-filter/